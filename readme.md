**NestedSets**

    #git clone https://gitlab.com/icqparty/TestNestedSets.git

    переходим в директорию проекта

    #cd TestNestedSets


_1. Запускаем docker контейнеры с php и mysql_
    
    # docker-composer up -d
    
    данные базы
    
    host: localhost
    root_password: root
    db_user: nested_sets
    db_password: nested_sets
    db_name: nested_sets
    
    накатываем бекап:
    
    # cat ./www/backup.sql | docker exec -i db-nested-sets /usr/bin/mysql -u root --password=root nested-sets

    
    открываем браузер : http://localhost:80

_2. Выполнить тесты_
    
    # docker exec -ti app-nested-sets ./vendor/bin/phpunit
    
    
    Отчет Code Coverege в диретории ./path/www/log/coverage/

    