<?php
/**
 * Created by IntelliJ IDEA.
 * User: icqparty
 * Date: 14.01.17
 * Time: 11:26
 */

namespace App;

use App\Controller\BaseController;

class Router
{

    private $routers = array();

    public function __construct($config = array())
    {
        if (!is_array($config)) {
            throw new \Exception('Not config is not Array');
        }

        if (count($config)>0) {

            foreach ($config as $name => $router) {
                $a = new $router['controller'];
                if (!$a instanceof BaseController)
                    throw new \Exception("{$router['controller']} is not instanceof \\App\\Controller\\BaseController");

                $this->routers[$name] = $router;
            }
        }
    }

    /**
     * @return string
     */
    public function dispach(): string
    {
        $url = $_SERVER['PATH_INFO'] ?? '/';

        if (!isset($this->routers[$url])) {
            return $this->error();
        }

        $params = array();

        if (!empty($_POST) || !empty($_GET)) {
            $params=array_merge($_POST,$_GET);
        }

        $controller = new $this->routers[$url]['controller'];
        $action = $this->routers[$url]['action'];

        $controller->setParams($params);

        return $controller->$action();
    }

    private function error()
    {
        return '404 Not found';
    }
}