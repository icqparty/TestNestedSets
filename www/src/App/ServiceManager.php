<?php
/**
 * Created by IntelliJ IDEA.
 * User: icqparty
 * Date: 15.01.17
 * Time: 13:34
 */

namespace App;


class  ServiceManager
{
    private static $services = NULL;


    //Регистрирует объект сервиса
    public static function get(string $name)
    {
        if (!isset(self::$services[$name])) {
            throw  new \Exception("Service {$name} not registry");
        }
        return self::$services[$name];
    }

    //Извлекает сервис по имени
    public static function set(string $name, $context)
    {
        if (is_null($context)) {
            throw  new \Exception("Service {$name} is null");
        }

        if (!isset(self::$services[$name])) {
            self::$services[$name] = $context;
        }
    }
}