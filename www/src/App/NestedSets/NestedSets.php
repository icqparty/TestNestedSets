<?php
/**
 * Created by IntelliJ IDEA.
 * User: julkey
 * Date: 13.01.17
 * Time: 19:36
 */

namespace App\NestedSets;


use App\Db;
use App\ServiceManager;

class NestedSets
{
    protected $db;
    public function __construct()
    {
        $this->tbl = 'tree';
        $this->db = ServiceManager::get('db');
    }

    // Добавить узел
    public function addNode(int $id, string $name)
    {
        $node = $this->getNode($id);
        $right_key = $node['right_key'];
        $level = $node['level'];

        $sql_update = "
			UPDATE  {$this->tbl} SET 
				right_key = right_key + 2, 
				left_key = IF(left_key > {$right_key}, left_key + 2, left_key) 
			WHERE right_key >= {$right_key}";
        $this->db->exec($sql_update);

        $sql_insert = "
			INSERT 
			INTO  {$this->tbl} 	
			SET left_key = {$right_key}, 
				right_key = {$right_key} + 1,
				`level` = {$level} + 1, 
				name = :name";

        $STH = $this->db->prepare($sql_insert);

        $STH->bindParam(':name', $name);
        $STH->execute();

        $id=$this->db->lastInsertId();
        return $id;
    }

    //переименование ноды
    public function renameNode(int $id, string $new_name): bool
    {
        $sql_update_name_node = "UPDATE {$this->tbl} SET name ='{$new_name}' WHERE id={$id}";
        $res = $this->db->exec($sql_update_name_node);

        if ($res > 0) {
            return true;
        }
        return false;
    }

    // Удалить узел
    public function removeNode(int $id)
    {
        $node = $this->getNode($id);
        $left_key = $node['left_key'];
        $right_key = $node['right_key'];

        $sql_delete = '
			DELETE FROM ' . $this->tbl . ' 
			WHERE 
				left_key >= ' . $left_key . ' AND 
				right_key <= ' . $right_key;
        $this->db->exec($sql_delete);

        $sql_update = "
			UPDATE  {$this->tbl} SET 
				left_key = IF(left_key > {$left_key}, left_key - ({$right_key} - {$left_key} + 1), left_key), 
				right_key = right_key - ({$right_key} - {$left_key} + 1) 
			WHERE right_key > {$right_key}";


        $this->db->exec($sql_update);
    }
    public function moveNode($id, $to_id)
    {
        $node = $this->getNode($id);
        $left_id = $node['left_key'];
        $right_id = $node['right_key'];
        $level = $node['level'];

        $node = $this->getNode($to_id);
        $left_to_id = $node['left_key'];
        $right_to_id = $node['right_key'];
        $level_to_id = $node['level'];

        if ($id == $to_id || $left_id == $left_to_id || ($left_to_id >= $left_id && $left_to_id <= $right_id) || ($level == $level_to_id + 1 && $left_id > $left_to_id && $right_id < $right_to_id)) {
            return false;
            //throw new \Exception('Node no be move', 0);
        }

        $sql = 'UPDATE ' . $this->tbl . ' SET ';
        if ($left_to_id < $left_id && $right_to_id > $right_id && $level_to_id < $level - 1) {
            $sql .= '`level` = CASE WHEN left_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN `level`' . sprintf('%+d', -($level - 1) + $level_to_id) . ' ELSE `level` END, ';
            $sql .= 'right_key = CASE WHEN right_key BETWEEN ' . ($right_id + 1) . ' AND ' . ($right_to_id - 1) . ' THEN right_key - ' . ($right_id - $left_id + 1) . ' ';
            $sql .= 'WHEN left_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN right_ky+' . ((($right_to_id - $right_id - $level + $level_to_id) / 2) * 2 + $level - $level_to_id - 1) . ' ELSE right_key END, ';
            $sql .= 'left_key = CASE WHEN left_key BETWEEN ' . ($right_id + 1) . ' AND ' . ($right_to_id - 1) . ' THEN left_key-' . ($right_id - $left_id + 1) . ' ';
            $sql .= 'WHEN left_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN left_key+' . ((($right_to_id - $right_id - $level + $level_to_id) / 2) * 2 + $level - $level_to_id - 1) . ' ELSE left_key END ';
            $sql .= 'WHERE left_key BETWEEN ' . ($left_to_id + 1) . ' AND ' . ($right_to_id - 1);
        } elseif ($left_to_id < $left_id) {
            $sql .= '`level` = CASE WHEN `level` BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN `level`' . sprintf('%+d', -($level - 1) + $level_to_id) . ' ELSE `level` END, ';
            $sql .= 'left_key = CASE WHEN left_key BETWEEN ' . $right_to_id . ' AND ' . ($left_id - 1) . ' THEN left_key+' . ($right_id - $left_id + 1) . ' ';
            $sql .= 'WHEN left_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN left_key-' . ($left_id - $right_to_id) . ' ELSE left_key END, ';
            $sql .= 'right_key = CASE WHEN right_key BETWEEN ' . $right_to_id . ' AND ' . $left_id . ' THEN right_key + ' . ($right_id - $left_id + 1) . ' ';
            $sql .= 'WHEN right_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN right_key - ' . ($left_id - $right_to_id) . ' ELSE right_key END ';
            $sql .= 'WHERE (left_key BETWEEN ' . $left_to_id . ' AND ' . $right_id . ' ';
            $sql .= 'OR right_key BETWEEN ' . $left_to_id . ' AND ' . $right_id . ')';
        } else {
            $sql .= '`level` = CASE WHEN left_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN `level`' . sprintf('%+d', -($level - 1) + $level_to_id) . ' ELSE `level` END, ';
            $sql .= 'left_key = CASE WHEN left_key BETWEEN ' . $right_id . ' AND ' . $right_to_id . ' THEN left_key-' . ($right_id - $left_id + 1) . ' ';
            $sql .= 'WHEN left_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN left_key+' . ($right_to_id - 1 - $right_id) . ' ELSE left_key END, ';
            $sql .= 'right_key = CASE WHEN right_key BETWEEN ' . ($right_id + 1) . ' AND ' . ($right_to_id - 1) . ' THEN right_key - ' . ($right_id - $left_id + 1) . ' ';
            $sql .= 'WHEN right_key BETWEEN ' . $left_id . ' AND ' . $right_id . ' THEN right_key+' . ($right_to_id - 1 - $right_id) . ' ELSE right_key END ';
            $sql .= 'WHERE (left_key BETWEEN ' . $left_id . ' AND ' . $right_to_id . ' ';
            $sql .= 'OR right_key BETWEEN ' . $left_id . ' AND ' . $right_to_id . ')';
        }

        $this->db->exec($sql);
        return true;
    }
    // Родительский узел
    private function parentNode(int $id)
    {
        $node = $this->getNode($id);
        $left_key = $node['left_key'];
        $right_key = $node['right_key'];
        $level = $node['level'] - 1;
        $sql = "
			SELECT id, name, level 
			FROM {$this->tbl} 
			WHERE 
				left_key <= {$left_key} AND 
				right_key >= {$right_key} AND
				level = {$level}
			ORDER BY left_key";

        $STH = $this->db->query($sql);
        return $STH->fetch(\PDO::FETCH_ASSOC);
    }

    // Получить узел
    private function getNode(int $id)
    {
        $sql_select_node = "SELECT * FROM {$this->tbl} 	WHERE id = :id";
        $sth = $this->db->prepare($sql_select_node);
        $sth->bindParam(':id', $id);
        $sth->execute();
        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    // Построение дерева
    public function getTree()
    {
        $sql_select_nodes = "SELECT `id`, `name`,  `level`  FROM {$this->tbl} ORDER BY left_key ";
        $STH = $this->db->query($sql_select_nodes);
        $result = [];
        $i=0;
        while ($row = $STH->fetch(\PDO::FETCH_ASSOC)) {

            $result[$i] = $row;
            $i++;
        }
        return $result;
    }
}