<?php
/**
 * Created by IntelliJ IDEA.
 * User: icqparty
 * Date: 14.01.17
 * Time: 11:26
 */

namespace App;


class Logger
{

    const INFO = 'info';
    const ERROR = 'error';

    protected $file_name = 'file.log';

    protected $hfile = null;

    public function __construct(string $file)
    {
        $this->file_name = $file;
    }

    public function log($type, string $message)
    {
        if (!is_resource($this->hfile)) {
            $this->hfile = @fopen($this->file_name, 'ab');
        }

        if (!is_resource($this->hfile)) {
            throw new \Exception('File no created');
        }

        if (!empty($message)) {
            $date = date('Y-m-d H:i:s');
            $content = "{$date} [{$type}] {$message}\n";
            fwrite($this->hfile, $content);
        }

        return true;
    }

    public function __destruct()
    {
        if (is_resource($this->hfile))
            fclose($this->hfile);
    }
}