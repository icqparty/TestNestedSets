<?php
namespace App\Controller;
/**
 * Created by IntelliJ IDEA.
 * User: icqparty
 * Date: 14.01.17
 * Time: 22:40
 */
class Index extends BaseController
{

    //Главная страница
    public function index()
    {
        return file_get_contents(__DIR__ . '/../../../template/index.html');
    }
}