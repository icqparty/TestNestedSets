<?php
namespace App\Controller;

use App\Logger;
use App\ServiceManager;

/**
 * Created by IntelliJ IDEA.
 * User: icqparty
 * Date: 14.01.17
 * Time: 22:40
 */
class Tree extends BaseController
{

    private $ns = null;
    private $logger= null;

    public function __construct()
    {
        parent::__construct();
        $this->ns = ServiceManager::get('nested-sets');
        $this->logger = ServiceManager::get('logger');
    }


    public function tree()
    {
        return json_encode($this->ns->getTree());
    }

    public function add()
    {
        $id = $this->getParam('id', false);
        $name = $this->getParam('name', false);

        if ($id && $name && !empty($id) && !empty($name)) {
            $id_new=$this->ns->addNode($id,$name);
            $this->logger->log(Logger::INFO,"Add to:{$id} new_id:{$id_new} {$name}");
            return json_encode(array('status' => '200', 'message' => "Add to {$id} new_id {$id_new} {$name}"));
        } else {
            return json_encode(array('status' => '500', 'message' => 'Params invalid'));
        }
    }

    public function remove()
    {
        $id = $this->getParam('id', false);
        if ($id && !empty($id)) {
            $this->ns->removeNode($id);
            $this->logger->log(Logger::INFO,"Remove: ID {$id}");
            return json_encode(array('status' => '200', 'message' => "Remove: ID {$id}"));
        } else {
            return json_encode(array('status' => '500', 'message' => 'Params invalid'));
        }
    }

    public function move()
    {
        $id = $this->getParam('id', false);
        $to_id = $this->getParam('to_id', false);

        if ($id && $to_id && !empty($id) && !empty($to_id)) {
            $this->ns->moveNode($id,$to_id);

            $this->logger->log(Logger::INFO,"Move: ID {$id} to  ID {$to_id}");
            return json_encode(array('status' => '200', 'message' => "Move: ID {$id} to  ID {$to_id}"));
        } else {
            return json_encode(array('status' => '500', 'message' => 'Params invalid'));
        }
    }

    public function rename()
    {
        $id = $this->getParam('id', false);
        $name = $this->getParam('name', false);

        if ($id && $name && !empty($id) && !empty($name)) {
            $this->ns->renameNode($id,$name);
            $this->logger->log(Logger::INFO,"Rename: ID {$id} to name  {$name}");
            return json_encode(array('status' => '200', 'message' => "Rename: ID {$id} to name  {$name}"));
        } else {
            return json_encode(array('status' => '500', 'message' => 'Params invalid'));
        }
    }
}