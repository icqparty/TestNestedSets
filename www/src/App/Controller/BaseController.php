<?php
/**
 * Created by IntelliJ IDEA.
 * User: icqparty
 * Date: 15.01.17
 * Time: 0:22
 */

namespace App\Controller;


abstract class BaseController
{

    protected $params = array();

    public function __construct()
    {

        //определяем Ajax устанавливаем заголовки ответа
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            header('Content-Type: application/json');
        }
    }


    //Устанавливает параеметры а также вильтрует их для безопасности
    public function setParams($params = array())
    {
        if (!is_array($params)) {
            throw new \Exception('Params not array');
        }

        if(count($params)>0){
            foreach ($params as $name=>$value){
                $str = $value;
                $str = trim($str);
                $str = stripslashes($str);
                $str = htmlspecialchars($str);
                $this->params[$name]=$str;
            }
        }
    }
    //Возращает значение по клюxe или значение по умолчанию
    public function getParam(string $name, $defaultValue = null)
    {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        }

        return $defaultValue;
    }
}