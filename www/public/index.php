<?php
require_once dirname(__DIR__) . '/vendor/autoload.php';

use App\ServiceManager;

$config = [
    'log' => dirname(__DIR__) . '/log/file.log',
    'db' => [
        'host' => $_SERVER['REMOTE_ADDR'],
        'dbname' => 'nested-sets',
        'username' => 'nested-sets',
        'password' => 'nested-sets'
    ],
    'routers' => [
        '/' => [
            'controller' => \App\Controller\Index::class, 'action' => 'index'
        ],
        '/nested-sets/tree' => [
            'controller' => App\Controller\Tree::class, 'action' => 'tree'
        ],
        '/nested-sets/add' => [
            'controller' => App\Controller\Tree::class, 'action' => 'add'
        ],
        '/nested-sets/remove' => [
            'controller' => App\Controller\Tree::class, 'action' => 'remove'
        ],
        '/nested-sets/move' => [
            'controller' => App\Controller\Tree::class, 'action' => 'move'
        ],
        '/nested-sets/rename' => [
            'controller' => App\Controller\Tree::class, 'action' => 'rename'
        ],
    ]
];


ServiceManager::set('db', new PDO("mysql:host={$config['db']['host']};dbname={$config['db']['dbname']}", $config['db']['username'], $config['db']['password']));
ServiceManager::set('logger', new \App\Logger($config['log']));
ServiceManager::set('nested-sets', new \App\NestedSets\NestedSets());

$router = new \App\Router($config['routers']);

echo $router->dispach();