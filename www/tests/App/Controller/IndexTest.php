<?php
namespace App;

use App\Controller\Index;
use PHPUnit\Framework\TestCase;

class IndexTest extends TestCase
{
    public function testReturnIndexAction()
    {
        $index = new Index();
        $this->assertEquals($index->index(), file_get_contents(dirname(__DIR__) . '/../../template/index.html'));
    }
}
