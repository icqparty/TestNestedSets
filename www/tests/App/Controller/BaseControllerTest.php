<?php
namespace App;

use App\Controller\Index;
use PHPUnit\Framework\TestCase;

class BaseControllerTest extends TestCase
{
    private $baseController = null;

    public function setUp()
    {
        parent::setUp();

        $this->baseController = $this->getMockForAbstractClass('App\\Controller\\BaseController');
    }

    public function testSetParamException()
    {
        $this->setExpectedException('Exception');

        $params = array();
        $this->baseController->setParams($params);

        $params = new \stdClass();

        $this->baseController->setParams($params);
    }

    public function testGetParam()
    {
        $params = [
            'test-param' => 'test-value'
        ];

        $this->baseController->setParams($params);

        $this->assertEquals('test-value', $this->baseController->getParam('test-param'));
    }

    public function testGetParamDefaultValue()
    {
        $params = [
            'test-param1' => 'test-value1'
        ];
        $this->baseController->setParams($params);

        $this->assertEquals('default-value', $this->baseController->getParam('default-name', 'default-value'));
    }

    public function testParamSecure()
    {
        $params = [
            'bad-param' => '<html>bad-param-value<html>'
        ];
        $this->baseController->setParams($params);

        $this->assertEquals('&lt;html&gt;bad-param-value&lt;html&gt;', $this->baseController->getParam('bad-param'));
    }
}
