<?php
namespace App;

use PHPUnit\Framework\TestCase;

class ServiceManagerTest extends TestCase
{

    private $serviceManager;

    public function testSetService()
    {
        ServiceManager::set('test_service1', 'test_service_value1');
        $this->assertEquals('test_service_value1', ServiceManager::get('test_service1'));
    }

    public function testSetNullService()
    {
        $this->setExpectedException('Exception');
        ServiceManager::set('null_service', null);
    }

    public function testGetNoExistService()
    {
        $this->setExpectedException('Exception');
        ServiceManager::get('not_exist_service');

    }

    public function testGetService()
    {
        ServiceManager::set('test_service2', 'test_service_value2');
        $this->assertEquals('test_service_value2', ServiceManager::get('test_service2'));
    }
}
