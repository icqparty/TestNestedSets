<?php
namespace App;

use App\Controller\BaseController;
use App\Controller\Index;
use App\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{

    public function testFound()
    {
        $router = new Router(['/' => [
            'controller' => \App\Controller\Index::class, 'action' => 'index'
        ]
        ]);


        $router->dispach();
    }


    public function testPageNotFound()
    {
        $router = new Router();
        $_SERVER['PATH_INFO'] = '/ffff';
        $this->assertEquals('404 Not found', $router->dispach());
    }

    public function testQueryParam()
    {
        $config = [
                '/test' => ['controller' => Index::class,'action'=>'index']
        ];

        $router = new Router($config);
        $_SERVER['PATH_INFO'] = '/test';
        $_POST['key_post']='key_post_value';
        $_GET['key_get']='key_get_value';
        $router->dispach();
    }

    public function testConfigNotArray()
    {

        $this->setExpectedException('Exception');
        new Router('');
    }

    public function testControllerNotInstanceOfClassBaseController()
    {
        $this->setExpectedException('Exception');
        new Router(['/' => [
            'controller' => \stdClass::class, 'action' => 'index'
        ]
        ]);
    }
}
