<?php
namespace App\NestedSet;

use App\NestedSets\NestedSets;
use App\ServiceManager;
use PHPUnit\Framework\TestCase;

class NestedSetsTest extends TestCase
{

    private $nestedSets = null;
    private $db = null;


    protected function setUp()
    {
        parent::setUp();

        $this->db = ServiceManager::get('db');

        $this->nestedSets = new NestedSets();
    }

    private function clearDb()
    {

        $this->db->exec("DELETE FROM tree;");
        $this->db->exec("ALTER TABLE tree AUTO_INCREMENT=0;");

        $sql_insert_test_data = "
            INSERT INTO tree (id,left_key,right_key,level,name) VALUES (1,1,1,0,'root');
        ";

        $this->db->exec($sql_insert_test_data);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->clearDb();
    }

    public function testAddNode()
    {
        $id = 1;
        $name = 'add_name';

        $id_new = $this->nestedSets->addNode($id, $name);
        $this->assertNotNull($id_new);

        $sql_select_node = "SELECT * FROM tree WHERE name='{$name}';";
        $STH = $this->db->query($sql_select_node);

        $this->assertEquals($name, $STH->fetch(\PDO::FETCH_ASSOC)['name']);
    }

    public function testRemoveNode()
    {
        $id = 1;
        $name = 'remove_name';

        $id_new = $this->nestedSets->addNode($id, $name);
        $this->nestedSets->removeNode($id_new);

        $sql_select_node = "SELECT * FROM tree WHERE id={$id_new};";
        $STH = $this->db->query($sql_select_node);

        $this->assertNull($STH->fetch(\PDO::FETCH_ASSOC)['name']);
    }

//    public function testMoveNode()
//    {
//        $id_sub_node1_2 = $this->nestedSets->addNode(1, 'sub_node1_2');
//        $id_sub_node2_3 = $this->nestedSets->addNode($id_sub_node1_2, 'sub_node2_3');
//
//        $this->nestedSets->moveNode($id_sub_node2_3, 1);
//
//        $expected = array(0 =>
//            array(
//                'id' => "1",
//                'name' => "root",
//                'level' => "0"
//            ),
//            1 => array(
//                'id' => "2",
//                'name' => "sub_node1_2",
//                'level' => "1",
//            ),
//            2 => array(
//                'id' => "4",
//                'name' => "sub_node2_3",
//                'level' => "2",
//            )
//        );
//
//
//        $actual=$this->nestedSets->getTree();
//        $this->assertEquals($expected, $actual);
//    }

    public function testRenameNode()
    {

        $id = 1;
        $name = 'rename';

        $this->nestedSets->renameNode($id, $name);


        $sql_select_node = "SELECT * FROM tree WHERE id={$id};";
        $STH = $this->db->query($sql_select_node);

        $this->assertEquals('rename', $STH->fetch(\PDO::FETCH_ASSOC)['name']);
    }

    public function testGetTree()
    {
        $this->nestedSets->getTree();
        $this->assertTrue(true);
    }
}
