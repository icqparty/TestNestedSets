/*
Navicat MySQL Data Transfer

Source Server         : nested-sets
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : nested-sets

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-01-17 01:56:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tree
-- ----------------------------
DROP TABLE IF EXISTS `tree`;
CREATE TABLE `tree` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `left_key` int(10) DEFAULT '0',
  `right_key` int(10) DEFAULT '0',
  `level` int(10) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `left_key` (`left_key`,`right_key`,`level`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tree
-- ----------------------------
INSERT INTO `tree` VALUES ('1', '1', '25', '1', 'root');
INSERT INTO `tree` VALUES ('2', '1', '2', '2', 'name1');
INSERT INTO `tree` VALUES ('3', '3', '10', '2', 'name1');
INSERT INTO `tree` VALUES ('4', '11', '16', '2', 'name1');
INSERT INTO `tree` VALUES ('5', '17', '18', '2', 'name1');
INSERT INTO `tree` VALUES ('6', '4', '7', '3', 'name3');
INSERT INTO `tree` VALUES ('7', '5', '6', '4', 'name6');
INSERT INTO `tree` VALUES ('8', '8', '9', '3', 'name3');
INSERT INTO `tree` VALUES ('9', '12', '13', '3', 'name4');
INSERT INTO `tree` VALUES ('10', '14', '15', '3', 'name4');
INSERT INTO `tree` VALUES ('11', '19', '20', '2', 'name1');
INSERT INTO `tree` VALUES ('12', '21', '22', '2', 'name1');
INSERT INTO `tree` VALUES ('13', '23', '24', '2', 'name1');
