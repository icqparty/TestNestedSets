FROM php:7.1.0-cli
RUN apt-get update && apt-get install -y git

RUN pecl install xdebug \
    && docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-enable xdebug pdo pdo_mysql

RUN mkdir /www
WORKDIR /www




